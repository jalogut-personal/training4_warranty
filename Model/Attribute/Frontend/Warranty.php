<?php
/**
 * Warranty.php
 *
 * @category  Training4
 * @package   Training4_Warranty
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */
namespace Training4\Warranty\Model\Attribute\Frontend;

class Warranty extends \Magento\Eav\Model\Entity\Attribute\Frontend\AbstractFrontend
{
    /**
     * Display warranty value in bold
     *
     * @param \Magento\Framework\DataObject $object
     *
     * @return string
     */
    public function getValue(\Magento\Framework\DataObject $object)
    {
        $value = parent::getValue($object);
        if ($value) {
            return "<strong>" . $value . "</strong>";
        }
        return "";
    }
}
