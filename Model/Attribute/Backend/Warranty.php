<?php
/**
 * Warranty.php
 *
 * @category  Training4
 * @package   Training4_Warranty
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */
namespace Training4\Warranty\Model\Attribute\Backend;

class Warranty extends \Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend
{
    /**
     * Convert value to string years before save
     *
     * @param \Magento\Framework\DataObject $object
     *
     * @return $this
     */
    public function beforeSave($object)
    {
        $attributeCode = $this->getAttribute()->getName();
        $value = $object->getData($attributeCode);
        if (is_numeric($value)) {
            $object->setData($attributeCode, $value . ' year(s)');
        }
        return $this;
    }
}
