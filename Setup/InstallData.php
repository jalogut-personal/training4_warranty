<?php
/**
 * InstallData.php
 *
 * @category  Training4
 * @package   Training4_Warranty
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */
namespace Training4\Warranty\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    /**
     * Category setup factory
     *
     * @var CategorySetupFactory
     */
    private $categorySetupFactory;

    /**
     * Construct
     *
     * @param \Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory
     */
    public function __construct(\Magento\Catalog\Setup\CategorySetupFactory $categorySetupFactory)
    {
        $this->categorySetupFactory = $categorySetupFactory;
    }

    /**
     * Install script
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '0.0.1') < 0) {
            $categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
            $entityTypeId = \Magento\Catalog\Model\Product::ENTITY;

            $categorySetup->addAttributeSet($entityTypeId, 'Gear', 2);
            $attributeSetId = $categorySetup->getAttributeSet($entityTypeId, 'Gear', 'attribute_set_id');

            $categorySetup->addAttribute(
                $entityTypeId,
                'warranty',
                [
                    'name'                     => 'Warranty',
                    'group'                    => 'Product Details',
                    'required'                 => false,
                    'type'                     => 'text',
                    'visible_on_front'         => true,
                    'is_html_allowed_on_front' => true,
                    'label'                    => 'Warranty',
                    'attribute_set_id'         => $attributeSetId,
                    'frontend'                 => 'Training4\Warranty\Model\Attribute\Frontend\Warranty',
                    'backend'                  => 'Training4\Warranty\Model\Attribute\Backend\Warranty'
                ]
            );
        }
    }
}